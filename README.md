# README #

JEasyCryptoMobile is an Android application for encrypting and decrypting text. It uses JEasyCrypto library.

The code has been originally created for course work of Open Source Software Development, University of Oulu by

* **(c) Atte Tihinen, student of Degree Program of Information Processing Science, University of Oulu, Finland.** 
* **(c) Tommi Puuper�, student of Degree Program of Information Processing Science, University of Oulu, Finland.** 

JEasyCryptoLib is a library and a collection of related applications enabling encryption and decryption of text using different methdods.

The code has been originally created for educational purposes by

* **(c) Antti Juustila, Degree Program of Information Processing Science, University of Oulu, Finland.**

### License ###

For the purpose of this course, all code is licensed under GPLv3, unless otherwise stated in a file.

### What is this repository for? ###

Repository includes two components: 

* **JEasyCryptoMobile**, an Android application which is using the cryptographic methods of the Lib
* **JEasyCryptoLib** which implements several crypto methods and is extensible with new methods.

### How do I get set up? ###

1. Clone the project from bitbucket.
2. Build the lib using build.sh or buildWin.bat, which exports it as a .jar file into JEasyCryptoMobile/app/libs folder.
3. Build and run JEasyCryptoMobile with Android Studio.

### Contribution guidelines ###
Push only files that belong to the project, no OS specific files
Commit message should be in imperative mode and descriptive. It should be no more then 54 characters
Test your changes before pushing
Update branch often
Work in your local branch (git checkout -b mylocal)
After issues resolved and the commit is ready provide a patch by running 
git format-patch
Provide the formatted patch to project members through email.
The review process will start and you will get comments on your patch.
Patch will be accepted or rejected.
If your patch is rejected, use comments provided to change it if applicable.
If your patch is accepted you can push changes to contribution branch.

### Coding guidlines ###

The code has issues and features to be done. In order for the best experience for us and you, we have a simple list of guidelines that have to be followed.

First of all, please, use only TABS, no spaces. You can turn on the view of tabs and spaces in your preferred IDE.

Nested loops should be have equivalent to where the code is, for example, if 4 tabs have been used and you need to add something inside the loop, add one more tab.

Only one variable per declaration, declarations such as int a, b are incorrect.

Variables are named with non-capital letters and must make sense.

If two or more words need to be combined into one variable name, start second variable without spacing with capital letter, for example, cryptoMatrix

For everything else, Google Java guideline is preferred that can be found here
http://google.github.io/styleguide/javaguide.html


### Who do I talk to? ###

* Atte Tihinen
attetihinen(at)gmail.com

* Tommi Puuper�
tpuupera(at)gmail.com

* Mikko Korhonen
mikkokorhonen77(at)gmail.com
