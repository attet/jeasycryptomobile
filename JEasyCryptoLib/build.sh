mkdir bin
javac -d bin src/easycrypto/*.java
cd bin
jar cvf ../../EasyCryptoLib.jar easycrypto/*.class
echo ...Updating Android client API jar file
mkdir ../../JEasyCryptoMobile/app/libs
cp ../../EasyCryptoLib.jar ../../JEasyCryptoMobile/app/libs
cd..