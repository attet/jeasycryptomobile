package easycrypto;

import easycrypto.EasyCryptoAPI.Result;
import easycrypto.EasyCryptoAPI.ResultCode;

class Rot13Method implements CryptoMethod {

	private static final String[] ALPHABETS = new String[] {"ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz" };

	@Override
	public Result encrypt(String toEncrypt) {
		char[] result = toEncrypt.toCharArray();

		for (int i = 0; i < toEncrypt.length(); i++) {
			char c = toEncrypt.charAt(i);

			for (int j = 0; j < ALPHABETS.length; j++) {
				String alphabet = ALPHABETS[j];
				int charIndex = alphabet.indexOf(c);

				if (charIndex != -1) {
					result[i] = alphabet.charAt((charIndex + 13) % 26);
				}
			}
		}

		return new Result(ResultCode.ESuccess, new String(result));
	}

	@Override
	public Result decrypt(String toDecrypt) {
		return encrypt(toDecrypt);
	}

	@Override
	public String method() {
		return "rot13";
	}

}
