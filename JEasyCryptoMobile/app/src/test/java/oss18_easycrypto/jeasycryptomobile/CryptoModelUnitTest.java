package oss18_easycrypto.jeasycryptomobile;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CryptoModelUnitTest {

    @Test
    public void testCryptoModelSetDecrypt() {
        CryptoModel model = new CryptoModel();
        model.setDecrypt(true);
        assertEquals(true, model.decryptSelected());
        model.setDecrypt(false);
        assertEquals(false, model.decryptSelected());
    }

    @Test
    public void testCryptoModelInitialValueOfDecryptSelected() {
        CryptoModel model = new CryptoModel();
        assertEquals(false, model.decryptSelected());
    }
}