package oss18_easycrypto.jeasycryptomobile;

import java.util.List;

public interface Crypter {
    void crypt(String text);
    void setDecrypt(boolean decrypt);
    void setCryptoMethod(String method);
    String cryptedText();
    boolean decryptSelected();
    List<String> cryptoMethods();
}
