package oss18_easycrypto.jeasycryptomobile;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainFragment extends Fragment implements Observer {

    private OnFragmentInteractionListener mListener = null;
    private CryptoModel mCryptoModel = null;
    private Button mCryptButton = null;
    private EditText mTextEditor = null;
    private TextView mOriginalTextView = null;
    private TextView mCryptedTextView = null;
    private Switch mCryptSwitcher = null;
    private Spinner mSpinner = null;

    public MainFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        MainFragment myFragment = new MainFragment();
        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mCryptoModel = new CryptoModel();
        mCryptoModel.addObserver(this);
        mCryptoModel.setCryptoMethod(mCryptoModel.cryptoMethods().get(0));
        mCryptButton = view.findViewById(R.id.button);
        mTextEditor = view.findViewById(R.id.edit_text);
        mOriginalTextView = view.findViewById(R.id.original_text);
        mCryptedTextView = view.findViewById(R.id.crypted_text);
        mCryptSwitcher = view.findViewById(R.id.switcher);
        mSpinner = view.findViewById(R.id.methods_spinner);

        List<String> list = mCryptoModel.cryptoMethods();

        for (int i = 0; i < list.size(); ++i) {
            list.set(i, list.get(i).toUpperCase());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter(view.getContext(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(dataAdapter);
        mCryptButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mCryptoModel.crypt(mTextEditor.getText().toString());
                mOriginalTextView.setText(mTextEditor.getText().toString());
            }
        });

        mCryptSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCryptoModel.setDecrypt(isChecked);

                if (isChecked) {
                    mCryptButton.setText(R.string.button_encrypt_text);
                    mCryptoModel.setDecrypt(false);

                } else {
                    mCryptButton.setText(R.string.button_decrypt_text);
                    mCryptoModel.setDecrypt(true);
                }
            }
        });

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                mCryptoModel.setCryptoMethod(mCryptoModel.cryptoMethods().get(arg2));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                mCryptoModel.setCryptoMethod(mCryptoModel.cryptoMethods().get(0));
            }

        });

        mTextEditor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (mTextEditor.getRight() - mTextEditor.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mTextEditor.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mCryptoModel = null;
    }

    @Override
    public void update(Observable o, Object arg) {
        mCryptedTextView.setText(mCryptoModel.cryptedText());
    }

    public interface OnFragmentInteractionListener { }
}
