package oss18_easycrypto.jeasycryptomobile;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import easycrypto.EasyCryptoAPI;

public class CryptoModel extends Observable implements Crypter  {

    private String mCryptoMethod = null;
    private String mCryptedText = null;
    private boolean mDecrypt = false;
    EasyCryptoAPI.Result mResult = null;

    @Override
    public void crypt(String text) {

        if (mDecrypt) {
            mResult = EasyCryptoAPI.decrypt(text, mCryptoMethod);
        } else {
            mResult = EasyCryptoAPI.encrypt(text, mCryptoMethod);
        }

        if (mResult != null && mResult.result() != null) {
            if (!(mResult.result().equals(mCryptedText))) {
                mCryptedText = mResult.result();
                setChanged();
                notifyObservers();
            }
        } else {
            mCryptedText = "";
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public void setDecrypt(boolean decrypt) {

        if (decrypt) {
            this.mDecrypt = true;
        } else {
            this.mDecrypt = false;
        }
    }

    @Override
    public void setCryptoMethod(String method) {
        this.mCryptoMethod = method;
    }

    @Override
    public String cryptedText() {
        return mCryptedText;
    }

    @Override
    public boolean decryptSelected() {
        return mDecrypt;
    }

    @Override
    public List<String> cryptoMethods() {

        int begin = 0;
        int end = 0;
        char delimiter = ',';
        List<String> methods = new ArrayList<String>();
        String cryptoMethods = EasyCryptoAPI.methods();

        while (end < cryptoMethods.length() - 1) {

            if (cryptoMethods.indexOf(delimiter, begin) != -1) {
                end = (cryptoMethods.indexOf(delimiter, begin));
            } else {
                end = cryptoMethods.length();
            }

            methods.add(cryptoMethods.substring(begin, end));

            if (end != cryptoMethods.length() - 1) {
                begin = end + 1;
            }
        }

        return methods;
    }
}
